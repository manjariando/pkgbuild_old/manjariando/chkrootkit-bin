# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgname=chkrootkit
pkgname=${_pkgname}-bin
extra=+b1
_pkgver=0.55-1
if [ "${extra}" = "" ]; then
pkgver=${_pkgver/-/.}
else
pkgver=${_pkgver/-/.}.${extra/+/}
fi
pkgrel=1
pkgdesc="Rootkit detector. The chkrootkit security scanner searches the local system for signs that it is infected with a 'rootkit'."
arch=('i686' 'x86_64')
url="http://www.chkrootkit.org"
license=('BSD-2-Clause')
makedepends=('imagemagick')
backup=('etc/cron.daily/chkrootkit')
options=('!strip' '!emptydirs')
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${_pkgname}/org.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/security.png")
source_i686=("http://cdn-fastly.deb.debian.org/debian/pool/main/c/${_pkgname}/${_pkgname}_${_pkgver}${extra}_i386.deb")
source_x86_64=("http://cdn-fastly.deb.debian.org/debian/pool/main/c/${_pkgname}/${_pkgname}_${_pkgver}${extra}_amd64.deb")
sha512sums=('96d5649f8e251c9b9770923e34c6c35d1c3c12d1d17d35b24fe63b4ab5788e0c259b226040f7be544130d857508d14f9ae04af58c9bf83ec5f2582498838a462'
            '6e4b279dbbc632e2e0a9805b4ed9ebacbaab429ce351a937c9d21fb77e2ecb47db9c0334ef0d3407fc9a9186f945a859b858a45c37576b87bdff002e6b65e82a')
sha512sums_i686=('f7d756dcca739eccfab3317fba22ac43601e57e147466859c941c1783ee1568bc869f4d10d24b49658ce0a3e55b9aaeb87440cba679cae15948a7cce80c6e78d')
sha512sums_x86_64=('8663481dfe1b562227190ddcc35ac77956a6928dc0cb166254c4bab8d570cbf08e036cb861f098bf57cbca3ea6dfd2505f0095ab829991e26718f55c18a23644')

_chkrootkit_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Security;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_chkrootkit_desktop" | tee org.${_pkgname}.desktop
}

package(){
    depends=('binutils' 'net-tools' 'openssh')

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    # Fix directories structure differencies
    cd "${pkgdir}"

    install -D -m644 "${pkgdir}/usr/share/doc/${_pkgname}/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    mkdir usr/bin 2> /dev/null; mv usr/sbin/* usr/bin; rm -rf usr/sbin

    # Appstream
    install -Dm644 "${srcdir}/org.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/org.${_pkgname}.desktop" \
        "${pkgdir}/usr/share/applications/org.${_pkgname}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/security.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${_pkgname}.png"
    done
}
